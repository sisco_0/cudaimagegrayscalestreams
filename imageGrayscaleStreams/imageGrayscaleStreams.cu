#include <cuda.h>
#include <nvToolsExt.h>
#include <cstdio>
#include <opencv2/opencv.hpp>
#include <vector>
#include <ctime>
#include <fstream>

using namespace cv;
using namespace std;

__global__ void colorToGrayscaleGPU(uchar *imageColor, uchar *imageGrayscale, size_t width, size_t height, int pixelsPerThread)
{
	size_t idx = blockIdx.x*blockDim.x+threadIdx.x;
	size_t pixelStart = pixelsPerThread*idx;
	if(pixelStart>=width*height) return;
	size_t pixelEnd =
			min(width*height,pixelsPerThread*(idx+1));
	for(int i=pixelStart;i<pixelEnd;i++)
	{
		size_t colorIdx = i*3;
		imageGrayscale[i] = 0.07*(float)imageColor[colorIdx]+
				0.72*(float)imageColor[colorIdx+1]+
				0.21*(float)imageColor[colorIdx+2];
	}
}

int main(int argc, char **argv)
{
	if(argc!=3)
	{
		printf("[INFO] Usage: %s image.jpg experimentOutput.txt\n",argv[0]);
		exit(0);
	}
	Mat colorImage;
	colorImage = imread(argv[1], CV_LOAD_IMAGE_COLOR);
	Mat colorImageFloat;
	if(!colorImage.data)
	{
		printf("[ERR] Couldn't open %s\n",argv[1]);
		exit(0);
	}

	ofstream experimentFile;
	experimentFile.open(argv[2]);
	if(!experimentFile.is_open())
	{
		printf("[ERR] Couldn't open %s\n",argv[2]);
		colorImage.release();
		exit(0);
	}
	experimentFile << "tpb,ppt,msecs" << endl;
	uchar *d_imageGrayscale0;
	uchar *d_imageGrayscale1;
	uchar *d_imageGrayscale2;
	uchar *d_imageGrayscale3;
	uchar *d_imageColor;
	size_t width = colorImage.cols, height = colorImage.rows;
	cudaMalloc((void **)&d_imageColor,sizeof(uchar)*height*width*3);
	cudaMemcpy(d_imageColor,colorImage.data,sizeof(uchar)*height*width*3,cudaMemcpyHostToDevice);
	//h_imageGrayscale = (float*)malloc(sizeof(float)*height*width);
	cudaMalloc((void **)&d_imageGrayscale0,sizeof(uchar)*height*width);
	cudaMalloc((void **)&d_imageGrayscale1,sizeof(uchar)*height*width);
	cudaMalloc((void **)&d_imageGrayscale2,sizeof(uchar)*height*width);
	cudaMalloc((void **)&d_imageGrayscale3,sizeof(uchar)*height*width);

	cudaStream_t misStreams[4];
	for(int i=0;i<4;i++)
		cudaStreamCreate(&misStreams[i]);
	dim3 blocks,grid;
	vector<int> threadsPerBlockV = {8,16,32,64,128,256,512,1024};
	vector<int> pixelsPerThreadV = {1,2,4,8,16,32,64,128,256,512,1024};
	int pixelsPerThread;
	size_t threadsPerBlock;
	clock_t t;
	float msecs;
	const int runsPerConfiguration = 8;
	for(int i=0;i<threadsPerBlockV.size();i++)
	{
		threadsPerBlock = threadsPerBlockV[i];
		for(int j=0;j<pixelsPerThreadV.size();j++)
		{
			msecs = 0.0;
			for(int k=0;k<runsPerConfiguration;k++)
			{
				pixelsPerThread = pixelsPerThreadV[j];
				//Start timer
				t = clock();
				//Call grayscale kernel
				blocks = dim3(threadsPerBlock);
				grid = dim3(ceil((float)height*(float)width/(float)threadsPerBlock/(float)pixelsPerThread));
				colorToGrayscaleGPU<<<grid,blocks,0,misStreams[0]>>>(d_imageColor,d_imageGrayscale0,width,height,pixelsPerThread);
				colorToGrayscaleGPU<<<grid,blocks,0,misStreams[1]>>>(d_imageColor,d_imageGrayscale1,width,height,pixelsPerThread);
				colorToGrayscaleGPU<<<grid,blocks,0,misStreams[2]>>>(d_imageColor,d_imageGrayscale2,width,height,pixelsPerThread);
				colorToGrayscaleGPU<<<grid,blocks,0,misStreams[3]>>>(d_imageColor,d_imageGrayscale3,width,height,pixelsPerThread);
				cudaDeviceSynchronize();

				//Stop timer
				t = clock() - t;
				msecs += ((float)t);
			}
			msecs = msecs/(float)CLOCKS_PER_SEC*1000.0/runsPerConfiguration;
			experimentFile << threadsPerBlock << "," << pixelsPerThread << "," << msecs << endl;
		}
	}
	experimentFile.close();
	return 0;
}
